<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        $users = [
            [
                'id'             => 1,
                'name'           => 'Admin',
                'email'          => 'admin@admin.com',
                'password'       => '$2y$10$f8MMu1SfQwv/wnrWB74Gv.RvfvrPVa93OOr9K.OFF3OEvESR9B4vu',
                'remember_token' => null,
            ],
        ];

        User::insert($users);
    }
}
