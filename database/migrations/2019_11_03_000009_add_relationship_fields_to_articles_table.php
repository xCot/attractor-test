<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToArticlesTable extends Migration
{
    public function up()
    {
        Schema::table('articles', function (Blueprint $table) {
            $table->unsignedInteger('category_id')->nullable();

            $table->foreign('category_id', 'category_fk_556077')->references('id')->on('categories');

            $table->unsignedInteger('user_id')->nullable();

            $table->foreign('user_id', 'user_fk_556078')->references('id')->on('users');
        });
    }
}
