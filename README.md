# attractor-test

## Requirements
Mysql 5.7 or higher

PHP 7.2 or higher

apache 2.4 or nginx 1.14

composer (latest version)

## Installation
``` bash
1. Download and extract the archive or clone this repository.

2. Create fresh database.

3. Run "cp .env.example .env" file to copy example file to .env

4. Edit your .env file in root folder with DB credentials:

DB_DATABASE=your_database_name
DB_USERNAME=your_user_name
DB_PASSWORD=your_database_password


5. Run "composer install" command.

6. Run "php artisan migrate --seed" command.

7. Run "php artisan key:generate" command.

8. Run "php artisan storage:link" command.

9. Run "php artisan serve" command.

10. Go to http://127.0.0.1:8000 and log in with these credentials:

Username: admin@admin.com
Password: password
```


## API requests:

http://127.0.0.1:8000/api/v1/articles

http://127.0.0.1:8000/api/v1/categories

http://127.0.0.1:8000/api/v1/users