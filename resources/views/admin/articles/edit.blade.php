@extends('layouts.admin')
@section('content')
<div class="content">

    <div class="row">
        <div class="col-lg-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ trans('global.edit') }} {{ trans('cruds.article.title_singular') }}
                </div>
                <div class="panel-body">

                    <form action="{{ route("admin.articles.update", [$article->id]) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="form-group {{ $errors->has('category_id') ? 'has-error' : '' }}">
                            <label for="category">{{ trans('cruds.article.fields.category') }}</label>
                            <select name="category_id" id="category" class="form-control select2">
                                @foreach($categories as $id => $category)
                                    <option value="{{ $id }}" {{ (isset($article) && $article->category ? $article->category->id : old('category_id')) == $id ? 'selected' : '' }}>{{ $category }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('category_id'))
                                <p class="help-block">
                                    {{ $errors->first('category_id') }}
                                </p>
                            @endif
                        </div>
                        <div class="form-group {{ $errors->has('user_id') ? 'has-error' : '' }}">
                            <label for="user">{{ trans('cruds.article.fields.user') }}</label>
                            <select name="user_id" id="user" class="form-control select2">
                                @foreach($users as $id => $user)
                                    <option value="{{ $id }}" {{ (isset($article) && $article->user ? $article->user->id : old('user_id')) == $id ? 'selected' : '' }}>{{ $user }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('user_id'))
                                <p class="help-block">
                                    {{ $errors->first('user_id') }}
                                </p>
                            @endif
                        </div>
                        <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                            <label for="title">{{ trans('cruds.article.fields.title') }}</label>
                            <input type="text" id="title" name="title" class="form-control" value="{{ old('title', isset($article) ? $article->title : '') }}">
                            @if($errors->has('title'))
                                <p class="help-block">
                                    {{ $errors->first('title') }}
                                </p>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.article.fields.title_helper') }}
                            </p>
                        </div>
                        <div class="form-group {{ $errors->has('descritption') ? 'has-error' : '' }}">
                            <label for="descritption">{{ trans('cruds.article.fields.descritption') }}</label>
                            <input type="text" id="descritption" name="descritption" class="form-control" value="{{ old('descritption', isset($article) ? $article->descritption : '') }}">
                            @if($errors->has('descritption'))
                                <p class="help-block">
                                    {{ $errors->first('descritption') }}
                                </p>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.article.fields.descritption_helper') }}
                            </p>
                        </div>
                        <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
                            <label for="image">{{ trans('cruds.article.fields.image') }}</label>
                            <div class="needsclick dropzone" id="image-dropzone">

                            </div>
                            @if($errors->has('image'))
                                <p class="help-block">
                                    {{ $errors->first('image') }}
                                </p>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.article.fields.image_helper') }}
                            </p>
                        </div>
                        <div>
                            <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
                        </div>
                    </form>


                </div>
            </div>

        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    Dropzone.options.imageDropzone = {
    url: '{{ route('admin.articles.storeMedia') }}',
    maxFilesize: 2, // MB
    maxFiles: 1,
    addRemoveLinks: true,
    headers: {
      'X-CSRF-TOKEN': "{{ csrf_token() }}"
    },
    params: {
      size: 2
    },
    success: function (file, response) {
      $('form').find('input[name="image"]').remove()
      $('form').append('<input type="hidden" name="image" value="' + response.name + '">')
    },
    removedfile: function (file) {
      file.previewElement.remove()
      if (file.status !== 'error') {
        $('form').find('input[name="image"]').remove()
        this.options.maxFiles = this.options.maxFiles + 1
      }
    },
    init: function () {
@if(isset($article) && $article->image)
      var file = {!! json_encode($article->image) !!}
          this.options.addedfile.call(this, file)
      file.previewElement.classList.add('dz-complete')
      $('form').append('<input type="hidden" name="image" value="' + file.file_name + '">')
      this.options.maxFiles = this.options.maxFiles - 1
@endif
    },
     error: function (file, response) {
         if ($.type(response) === 'string') {
             var message = response //dropzone sends it's own error messages in string
         } else {
             var message = response.errors.file
         }
         file.previewElement.classList.add('dz-error')
         _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')
         _results = []
         for (_i = 0, _len = _ref.length; _i < _len; _i++) {
             node = _ref[_i]
             _results.push(node.textContent = message)
         }

         return _results
     }
}
</script>
@stop