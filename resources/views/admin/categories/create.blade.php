@extends('layouts.admin')
@section('content')
<div class="content">

    <div class="row">
        <div class="col-lg-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ trans('global.create') }} {{ trans('cruds.category.title_singular') }}
                </div>
                <div class="panel-body">

                    <form action="{{ route("admin.categories.store") }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                            <label for="title">{{ trans('cruds.category.fields.title') }}</label>
                            <input type="text" id="title" name="title" class="form-control" value="{{ old('title', isset($category) ? $category->title : '') }}">
                            @if($errors->has('title'))
                                <p class="help-block">
                                    {{ $errors->first('title') }}
                                </p>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.category.fields.title_helper') }}
                            </p>
                        </div>
                        <div class="form-group {{ $errors->has('parent') ? 'has-error' : '' }}">
                            <label for="parent">{{ trans('cruds.category.fields.parent') }}</label>
                            <input type="number" id="parent" name="parent" class="form-control" value="{{ old('parent', isset($category) ? $category->parent : '') }}" step="1">
                            @if($errors->has('parent'))
                                <p class="help-block">
                                    {{ $errors->first('parent') }}
                                </p>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.category.fields.parent_helper') }}
                            </p>
                        </div>
                        <div>
                            <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
                        </div>
                    </form>


                </div>
            </div>

        </div>
    </div>
</div>
@endsection